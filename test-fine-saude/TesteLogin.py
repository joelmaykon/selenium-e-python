# -*- coding: utf-8 -*-
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time


class PythonTeste(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()

    def test_login_inicio(self):
        driver = self.driver
        driver.get("https://finesaude2.herokuapp.com/")
        print(driver.title)
        self.assertIn("Fine Saúde", driver.title)
        email = driver.find_element_by_xpath('//*[@id="app"]/div/main/div/div/div/div[3]/div/div/span/div/div/form/div[1]/div/div/div[1]/div[1]/input')
        email.send_keys("nutricionista@email.com")
        senha = driver.find_element_by_xpath('//*[@id="app"]/div/main/div/div/div/div[3]/div/div/span/div/div/form/div[2]/div/div/div[1]/div[1]/input')
        senha.send_keys("123")
        driver.find_element_by_xpath('//*[@id="app"]/div/main/div/div/div/div[3]/div/div/span/div/div/form/div[3]/button/div').click()
        time.sleep(5)
        inicio = driver.find_element_by_xpath('//*[@id="app"]/div[22]/main/div/div/aside/div[2]/div[1]/a/div[2]')
        print(inicio.text)
        self.assertEqual("Início", inicio.text)        
        assert "No results found." not in driver.page_source
    
    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()