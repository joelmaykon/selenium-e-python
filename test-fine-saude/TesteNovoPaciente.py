# -*- coding: utf-8 -*-
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time


class PythonTeste(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()

    def test_login_inicio(self):
        driver = self.driver
        driver.get("https://finesaude2.herokuapp.com/")
        print(driver.title)
        self.assertIn("Fine Saúde", driver.title)
        email = driver.find_element_by_xpath('//*[@id="app"]/div/main/div/div/div/div[3]/div/div/span/div/div/form/div[1]/div/div/div[1]/div[1]/input')
        email.send_keys("nutricionista@email.com")
        time.sleep(2)
        senha = driver.find_element_by_xpath('//*[@id="app"]/div/main/div/div/div/div[3]/div/div/span/div/div/form/div[2]/div/div/div[1]/div[1]/input')
        senha.send_keys("123")
        driver.find_element_by_xpath('//*[@id="app"]/div/main/div/div/div/div[3]/div/div/span/div/div/form/div[3]/button/div').click()
        time.sleep(2)
        novo = driver.find_element_by_xpath('//*[@id="app"]/div[21]/main/div/div/div/div/div/div[1]/div/div/div/button/div/i')
        print(novo.text)
        self.assertEqual("add", novo.text)     
        driver.find_element_by_css_selector('#app > div.application--wrap > main > div > div > div > div > div > div.flex.xs12.sm12.md11.lg11.offset-md1.offset-lg1 > div > div > div > button').click()  
        time.sleep(5)
        pesquisa_paciente = driver.find_element_by_xpath('//*[@id="app"]/div[19]/div/div/div/div/div[1]/div[2]/div/div/div[1]/div[1]/div[1]/input')
        pesquisa_paciente.send_keys('Sonia')
        time.sleep(5)
        pesquisa_sonia = driver.find_element_by_xpath('//*[@id="app"]/div[20]/div/div/div/a/div/div/a/div')
        print("Nome do paciente: ",pesquisa_sonia.text)
        sonia = driver.find_element_by_xpath('//*[@id="app"]/div[21]/main/div/div/div/div/div/div[2]/div/div/div[1]/div/div/div[15]/div/div[1]/div[2]/text()[1]')
        self.assertEqual('Sonia', sonia.text)
        if(pesquisa_sonia.text == 'Sonia'):
            driver.find_element_by_xpath('//*[@id="app"]/div[20]/div/div/div/a/div/div/a/div').click()
            driver.find_element_by_xpath('//*[@id="app"]/div[19]/div/div/div/div/div[2]/div/button[2]/div').click()
            time.sleep(5)
            
            
        else:
            print("Não é o valor esperado ")
        
        time.sleep(30)
        assert "No results found." not in driver.page_source

    def tearDown(self):
        
        self.driver.close()

if __name__ == "__main__":
    unittest.main()